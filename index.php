<?php 

require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");

echo "Nama :" .$sheep->name. "<br>"; // "shaun"
echo "Legs :" .$sheep->legs. "<br>"; // 2
echo "Cold Blooded : " ;
var_dump($sheep->cold_blooded); // false
echo "<br><br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo "<br>";
echo "Nama :" .$kodok->name. "<br>"; 
echo "Legs :" .$kodok->legs. "<br>"; 
echo "Cold Blooded : " ;
var_dump($kodok->cold_blooded);
echo "<br><br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell() ; // "hop hop"
echo "<br>";
echo "Nama :" .$sungokong->name. "<br>"; 
echo "Legs :" .$sungokong->legs. "<br>"; 
echo "Cold Blooded : " ;
var_dump($sungokong->cold_blooded);  
 ?>